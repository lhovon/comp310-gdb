#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct Person {
	char* name;
	char* dob; 	
};

struct Person* create_person(char* name, char* dob) 
{
	// Allocate memory
	struct Person* p = malloc(sizeof(struct Person));
	
	// Copy values
	strcpy(p->name, name);
	strcpy(p->dob, dob);

	return p;
}

int days_til_bday(struct Person* p) 
{
	// Obtain current time
	time_t now;
	// time() places the current time in now
	time(&now);

	// Print current day
	printf("Today is %s", ctime(&now));
	
	// Parse the date of birth storing year, month and day
	int year, month, day;
    sscanf(p->dob, "%d-%d-%d", &year, &month, &day);
	
	// Create a struct to hold the birthdate
	struct tm* dob = localtime(&now);
	// Replace current month and day with birth month and day
	dob->tm_mon = month - 1;
	dob->tm_mday = day;
	
	// difftime returns the seconds between two times as a double
	double secs_til_bday = difftime(mktime(dob), now);
	// Convert to number of days
	int days_til_bday = secs_til_bday / (3600 * 24); 
	
	// Correct for negative days
	if (days_til_bday < 0) 
		days_til_bday = days_til_bday + 365;

	return days_til_bday;
}

void print_message(struct Person* p) 
{
	int days = days_til_bday(p);
	
	if (days == 0)
		printf("Happy Birthday %s!!!!\n", p->name);
	else 
		printf("Hi %s, there are %d days left until your birthday!\n", p->name, days);
}

int main(int argc, char** argv) {

	if (argc != 3) {
		printf("Usage: %s <your name> <date of birth YYYY-mm-dd>\n", argv[0]);
		exit(0);
	}

	char* name = argv[1];
	char* dob = argv[2];

	struct Person* person = create_person(name, dob);

	print_message(person);

	exit(0);
}

