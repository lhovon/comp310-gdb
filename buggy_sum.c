#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {

	if (argc < 2) 
	{	
		printf("Sums numbers given on the command line\n");
		printf("Usage: %s <num1> <num2> ...\n", argv[0]);
		exit(1);
	}
	
	int sum = 0;
	
	for (int i=0; i <= argc; i++)
	{
		// atoi converts a string to an int
		char* current_arg = argv[i];
		sum += atoi(current_arg);
	}

	printf("Sum: %d\n", sum);
}
